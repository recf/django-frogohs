from common import *

DEBUG = True
TEMPLATE_DEBUG = DEBUG


ADMINS = () # No emails in dev

MANAGERS = ADMINS


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'frogohs_dev',                      # Or path to database file if using sqlite3.
        'USER': 'frogohs_user',                      # Not used with sqlite3.
        'PASSWORD': 'user_frogohs',                  # Not used with sqlite3.
        'HOST': 'localhost',                      # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '',                      # Set to empty string for default. Not used with sqlite3.
    }
}



# Django registration settings
ACCOUNT_ACTIVATION_DAYS = 7

MIDDLEWARE_CLASSES += (
    'debug_toolbar.middleware.DebugToolbarMiddleware',
)

INSTALLED_APPS += (
    'debug_toolbar',
)

INTERNAL_IPS = ('127.0.0.1',)

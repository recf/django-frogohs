from django.conf.urls.defaults import patterns, include, url

import views

urlpatterns = patterns('',
    url(r'^(?P<username>\w+)/$',
        views.ProfileDetailView.as_view(),
        name='collmgt-profile-detail'),
        
    url(r'^(?P<username>\w+)/games/create/$',
        views.ProfileGameCreateView.as_view(),
        name='collmgt-profilegame-create'),
    url(r'^(?P<username>\w+)/games/(?P<profile_game_id>\d+)/$',
        views.ProfileGameDetailView.as_view(),
        name='collmgt-profilegame-detail'),
    url(r'^(?P<username>\w+)/games/(?P<profile_game_id>\d+)/update/$',
        views.ProfileGameUpdateView.as_view(),
        name='collmgt-profilegame-update'),
    url(r'^(?P<username>\w+)/games/(?P<profile_game_id>\d+)/delete/$',
        views.ProfileGameDeleteView.as_view(),
        name='collmgt-profilegame-delete'),
                
    url(r'^(?P<username>\w+)/games/$',
        views.ProfileGameListView.as_view(),
        kwargs = { 'ownership_statuses' : 'owned+unowned' },
        name='collmgt-profilegame-list'),
    url(r'^(?P<username>\w+)/games/by-status/all/$',
        views.ProfileGameListView.as_view(),
        name='collmgt-profilegame-list-bystatus-all'),
    url(r'^(?P<username>\w+)/games/by-status/(?P<ownership_statuses>[\w\+]+)/$',
        views.ProfileGameListView.as_view(),
        name='collmgt-profilegame-list-bystatus'),
)
from django.contrib import admin
from collmgt.models import Profile, OwnershipStatus


class ProfileAdmin(admin.ModelAdmin):
    list_display = ('user', )
    
class OwnershipStatusAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("name",)}    
    list_display = ('name', 'slug',  )  
    
admin.site.register(Profile, ProfileAdmin)
admin.site.register(OwnershipStatus, OwnershipStatusAdmin)
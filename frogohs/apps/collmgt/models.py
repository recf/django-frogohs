from django.db import models
from django.db.models.signals import post_save

from django.contrib.auth.models import User

from vgdb.models import PlatformDistMethod

class Profile(models.Model):
    user = models.OneToOneField(User, related_name='collmgt_profile')
    
    def __unicode__(self):
        return 'Profile for %(user)s' % { 'user' : self.user }

class OwnershipStatus(models.Model):
    name = models.CharField(max_length=50, unique=True)
    slug = models.SlugField(max_length=50, unique=True)
    
    def __unicode__(self):
        return self.name
    
    class Meta:
        ordering = ('name',)

class ProfileGame(models.Model):
    profile = models.ForeignKey(Profile)
    game_name = models.CharField(max_length=50, blank=False)
    platform_distmethod = models.ForeignKey(PlatformDistMethod, null=True, blank=True)
    ownership_status = models.ForeignKey(OwnershipStatus)

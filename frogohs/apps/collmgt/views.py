# from django.contrib.auth.decorators import user_passes_test
# from django.utils.decorators import method_decorator
from django.core.exceptions import PermissionDenied

from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView

from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse

from django.contrib.auth.models import User

from models import Profile, ProfileGame
from forms import ProfileGameForm

class ProfileMixin(object):
    username_url_kwarg = 'username'
    require_owner = False;
    
    def get_username(self):
        return self.kwargs.get(self.username_url_kwarg, None)
    
    def get_profile(self):
        user = User.objects.get(username=self.get_username)
        
        profile, created = Profile.objects.get_or_create(user=user)

        return profile
    
    def get_context_data(self, **kwargs):
        ctx = super(ProfileMixin, self).get_context_data(**kwargs)
        profile = self.get_profile()
        ctx['profile'] = profile
        is_owner = profile.user == self.request.user

        ctx['can_add'] = is_owner
        ctx['can_change'] = is_owner
        ctx['can_delete'] = is_owner
        
        if (self.require_owner and not is_owner):
            raise PermissionDenied
        
        return ctx

class ProfileDetailView(ProfileMixin, DetailView):
    model = Profile
    context_object_name = 'profile'
    
    # Override to create the profile if it doesn't exist
    def get_object(self, queryset=None): 
        return self.get_profile()

# Profile Games

# class ProfileGameMixin(ProfileMixin):
    # model = ProfileGame
    # form_class = ProfileGameForm
    
    # def get_queryset(self):
        # return self.get_profile().profilegame_set
    
class ProfileGameListView(ProfileMixin, ListView):
    model = ProfileGame
    context_object_name = 'profilegames'
    
    def get_queryset(self):
        ownership_statuses_slug = self.kwargs.get('ownership_statuses')
        
        qs = self.get_profile().profilegame_set
        if ownership_statuses_slug:
            ownership_statuses = ownership_statuses_slug.split('+')
            qs = qs.filter(ownership_status__slug__in=ownership_statuses)
            
        return qs

class ProfileGameDetailView(ProfileMixin, DetailView):
    model = ProfileGame
    pk_url_kwarg='profile_game_id'

class ProfileGameCreateView(ProfileMixin, CreateView):
    model = ProfileGame
    form_class = ProfileGameForm
    require_owner = True
        
    def get_success_url(self):
        return reverse('collmgt-profilegame-detail', \
            kwargs={'username':self.get_username(), \
                    'profile_game_id': self.object.id, \
                    })
    
    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.profile = self.get_profile()
        self.object.save()
        
        return HttpResponseRedirect(self.get_success_url())
        

class ProfileGameUpdateView(ProfileMixin, UpdateView):
    model = ProfileGame
    form_class = ProfileGameForm
    pk_url_kwarg = 'profile_game_id'
    require_owner = True
    
    def get_success_url(self):
        return reverse('collmgt-profilegame-detail', \
            kwargs={'username':self.get_username(), \
                    'profile_game_id': self.object.id, \
                    })
        
class ProfileGameDeleteView(ProfileMixin, DeleteView):
    model = ProfileGame
    pk_url_kwarg='profile_game_id'
    require_owner = True
        
    def get_success_url(self):
        return reverse('collmgt-profile-detail', \
            kwargs={'username':self.get_username(), \
                    })
from django.forms import ModelForm
from models import ProfileGame

class ProfileGameForm(ModelForm):
    class Meta:
        model = ProfileGame
        exclude = ('profile', )

from django.contrib import admin
from frogohs.apps.vgdb.models import Platform, \
                                     DistMethod, \
                                     PlatformDistMethod, \
                                     Series, \
                                     Game, \
                                     GameContent, \
                                     Region, \
                                     Release

class PlatformDistMethodInline(admin.TabularInline):
    model = PlatformDistMethod
    extra = 1

class PlatformAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("name",)}
    list_display = ('name', 'slug', 'dist_method_list', )  

    inlines = [PlatformDistMethodInline]
    
    def dist_method_list(self, platform):
        return ", ".join( [d.dist_method.name for d in platform.dist_method_set.all()] )

class DistMethodAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("name",)}
    list_display = ('name', 'slug', 'platform_list', )
    
    inlines = [PlatformDistMethodInline]
    
    def platform_list(self, dist_method):
        return ", ".join( [p.platform.name for p in dist_method.platform_set.all()] )
    
class SeriesInline(admin.TabularInline):
    model = Series
    extra = 1

class GameInline(admin.TabularInline):
    model = Game
    extra = 1
    
class SeriesAdmin(admin.ModelAdmin):    
    list_display = ('name', 'parent_series',)
    
    inlines = [SeriesInline, GameInline]

class GameContentInline(admin.TabularInline):
    model = GameContent
    extra = 1

class GameAdmin(admin.ModelAdmin):
    list_display = ('name', 'series', 'content_list',)
    
    inlines = [GameContentInline]

    def content_list(self, game):
        return ", ".join( [gc.name for gc in game.gamecontent_set.all()] )

class GameContentAdmin(admin.ModelAdmin):
    list_display = ('name', 'game',)

class RegionAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("name",)}
    list_display = ('name', 'slug',)

class ReleaseAdmin(admin.ModelAdmin):
    list_display = ('name', 'release_date',)
    
admin.site.register(Platform, PlatformAdmin)
admin.site.register(DistMethod, DistMethodAdmin)
admin.site.register(Series, SeriesAdmin)
admin.site.register(Game, GameAdmin)
admin.site.register(GameContent, GameContentAdmin)
admin.site.register(Region, RegionAdmin)
admin.site.register(Release, ReleaseAdmin)


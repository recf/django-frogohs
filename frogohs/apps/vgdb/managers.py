from django.db import models

class PlatformManager(models.Manager):
    def get_by_natural_key(self, slug):
        return self.get(slug=slug)
        
class DistMethodManager(models.Manager):
    def get_by_natural_key(self, slug):
        return self.get(slug=slug)
# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Game'
        db.create_table('vgdb_game', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('series', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['vgdb.Series'], null=True, blank=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=50)),
        ))
        db.send_create_signal('vgdb', ['Game'])

        # Adding field 'GameContent.game'
        db.add_column('vgdb_gamecontent', 'game',
                      self.gf('django.db.models.fields.related.ForeignKey')(to=orm['vgdb.Game'], null=True),
                      keep_default=False)

        # Adding field 'GameContent.name'
        db.add_column('vgdb_gamecontent', 'name',
                      self.gf('django.db.models.fields.CharField')(max_length=50, null=True),
                      keep_default=False)

        # Adding field 'GameContent.min_players'
        db.add_column('vgdb_gamecontent', 'min_players',
                      self.gf('django.db.models.fields.IntegerField')(default=1),
                      keep_default=False)

        # Adding field 'GameContent.max_players'
        db.add_column('vgdb_gamecontent', 'max_players',
                      self.gf('django.db.models.fields.IntegerField')(default=1),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting model 'Game'
        db.delete_table('vgdb_game')

        # Deleting field 'GameContent.game'
        db.delete_column('vgdb_gamecontent', 'game_id')

        # Deleting field 'GameContent.name'
        db.delete_column('vgdb_gamecontent', 'name')

        # Deleting field 'GameContent.min_players'
        db.delete_column('vgdb_gamecontent', 'min_players')

        # Deleting field 'GameContent.max_players'
        db.delete_column('vgdb_gamecontent', 'max_players')


    models = {
        'vgdb.distmethod': {
            'Meta': {'ordering': "['name']", 'object_name': 'DistMethod'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '50'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'})
        },
        'vgdb.game': {
            'Meta': {'ordering': "['series', 'name']", 'object_name': 'Game'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'series': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['vgdb.Series']", 'null': 'True', 'blank': 'True'})
        },
        'vgdb.gamecontent': {
            'Meta': {'ordering': "['title', 'subtitle']", 'unique_together': "(('title', 'subtitle'),)", 'object_name': 'GameContent'},
            'game': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['vgdb.Game']", 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'max_players': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'min_players': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True'}),
            'series': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['vgdb.Series']", 'null': 'True', 'blank': 'True'}),
            'subtitle': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'vgdb.platform': {
            'Meta': {'ordering': "['name']", 'object_name': 'Platform'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '50'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'})
        },
        'vgdb.platformdistmethod': {
            'Meta': {'ordering': "['platform', 'dist_method']", 'unique_together': "(('platform', 'dist_method'),)", 'object_name': 'PlatformDistMethod'},
            'dist_method': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'platform_set'", 'to': "orm['vgdb.DistMethod']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_transferrable': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'platform': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'dist_method_set'", 'to': "orm['vgdb.Platform']"})
        },
        'vgdb.region': {
            'Meta': {'ordering': "['name']", 'object_name': 'Region'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '50'}),
            'slug': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '10'})
        },
        'vgdb.release': {
            'Meta': {'object_name': 'Release'},
            'boxart': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'platform_distmethod': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['vgdb.PlatformDistMethod']"}),
            'region': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['vgdb.Region']"}),
            'release_date': ('django.db.models.fields.DateField', [], {})
        },
        'vgdb.series': {
            'Meta': {'ordering': "['name']", 'object_name': 'Series'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '50'}),
            'parent_series': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'subseries_set'", 'null': 'True', 'to': "orm['vgdb.Series']"})
        }
    }

    complete_apps = ['vgdb']
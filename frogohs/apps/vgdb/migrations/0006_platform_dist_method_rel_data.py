# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import DataMigration
from django.db import models

class Migration(DataMigration):

    RELATIONSHIPS = (
        ('xbox-360', 'physical-media', True,),
        ('xbox-360', 'xblm', False,),
        
        ('ps3', 'physical-media', True,),
        ('ps3', 'psn', False,),
        
        ('wii', 'physical-media', True,),
        ('wii', 'wii-shop-channel', False,),
        
        ('wii-u', 'physical-media', True,),
        ('wii-u', 'nintendo-eshop', False,),
        
        ('windows', 'physical-media', False,),
        ('windows', 'dev-website', False,),
        ('windows', 'steam', False,),
        ('windows', 'desura', False,),
        ('windows', 'gog', False,),
        ('windows', 'origin', False,),
        
        ('mac', 'physical-media', False,),
        ('mac', 'dev-website', False,),
        ('mac', 'steam', False,),
        ('mac', 'desura', False,),
        ('mac', 'mac-app-store', False,),
        
        ('linux', 'dev-website', False,),
        ('linux', 'desura', False,),
        
        ('ds', 'physical-media', True,),
        ('ds', 'dsi-shop', True,),
        
        ('3ds', 'physical-media', True,),
        ('3ds', 'nintendo-eshop', False,),
        
        ('psp', 'physical-media', True,),
        ('psp', 'psn', False,),
        
        ('vita', 'physical-media', True,),
        ('vita', 'psn', False,),
    )

    def forwards(self, orm):
        "Write your forwards methods here."
        # Note: Remember to use orm['appname.ModelName'] rather than "from appname.models..."

        for rel in self.RELATIONSHIPS:
            p = orm.Platform.objects.get(slug=rel[0])
            d = orm.DistMethod.objects.get(slug=rel[1])
            orm.PlatformDistMethod(platform=p, dist_method=d, is_transferrable=rel[2]).save()
        
    def backwards(self, orm):
        "Write your backwards methods here."
        for rel in self.RELATIONSHIPS:
            orm.PlatformDistMethod.objects.filter(platform__slug=rel[0], dist_method__slug=rel[1]).delete()

    models = {
        'vgdb.distmethod': {
            'Meta': {'ordering': "['name']", 'object_name': 'DistMethod'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '50'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'})
        },
        'vgdb.platform': {
            'Meta': {'ordering': "['name']", 'object_name': 'Platform'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '50'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'})
        },
        'vgdb.platformdistmethod': {
            'Meta': {'ordering': "['platform', 'dist_method']", 'unique_together': "(('platform', 'dist_method'),)", 'object_name': 'PlatformDistMethod'},
            'dist_method': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'platform_set'", 'to': "orm['vgdb.DistMethod']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_transferrable': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'platform': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'dist_method_set'", 'to': "orm['vgdb.Platform']"})
        }
    }

    complete_apps = ['vgdb']
    symmetrical = True

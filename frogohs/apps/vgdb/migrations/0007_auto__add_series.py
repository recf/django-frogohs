# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Series'
        db.create_table('vgdb_series', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('parent_series', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='subseries_set', null=True, to=orm['vgdb.Series'])),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=50)),
        ))
        db.send_create_signal('vgdb', ['Series'])


    def backwards(self, orm):
        # Deleting model 'Series'
        db.delete_table('vgdb_series')


    models = {
        'vgdb.distmethod': {
            'Meta': {'ordering': "['name']", 'object_name': 'DistMethod'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '50'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'})
        },
        'vgdb.platform': {
            'Meta': {'ordering': "['name']", 'object_name': 'Platform'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '50'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'})
        },
        'vgdb.platformdistmethod': {
            'Meta': {'ordering': "['platform', 'dist_method']", 'unique_together': "(('platform', 'dist_method'),)", 'object_name': 'PlatformDistMethod'},
            'dist_method': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'platform_set'", 'to': "orm['vgdb.DistMethod']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_transferrable': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'platform': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'dist_method_set'", 'to': "orm['vgdb.Platform']"})
        },
        'vgdb.series': {
            'Meta': {'ordering': "['name']", 'object_name': 'Series'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '50'}),
            'parent_series': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'subseries_set'", 'null': 'True', 'to': "orm['vgdb.Series']"})
        }
    }

    complete_apps = ['vgdb']
# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'PlatformDistMethod'
        db.create_table('vgdb_platformdistmethod', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('platform', self.gf('django.db.models.fields.related.ForeignKey')(related_name='dist_method_set', to=orm['vgdb.Platform'])),
            ('dist_method', self.gf('django.db.models.fields.related.ForeignKey')(related_name='platform_set', to=orm['vgdb.DistMethod'])),
            ('is_transferrable', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('vgdb', ['PlatformDistMethod'])

        # Adding unique constraint on 'PlatformDistMethod', fields ['platform', 'dist_method']
        db.create_unique('vgdb_platformdistmethod', ['platform_id', 'dist_method_id'])


    def backwards(self, orm):
        # Removing unique constraint on 'PlatformDistMethod', fields ['platform', 'dist_method']
        db.delete_unique('vgdb_platformdistmethod', ['platform_id', 'dist_method_id'])

        # Deleting model 'PlatformDistMethod'
        db.delete_table('vgdb_platformdistmethod')


    models = {
        'vgdb.distmethod': {
            'Meta': {'ordering': "['name']", 'object_name': 'DistMethod'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '50'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'})
        },
        'vgdb.platform': {
            'Meta': {'ordering': "['name']", 'object_name': 'Platform'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '50'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'})
        },
        'vgdb.platformdistmethod': {
            'Meta': {'ordering': "['platform', 'dist_method']", 'unique_together': "(('platform', 'dist_method'),)", 'object_name': 'PlatformDistMethod'},
            'dist_method': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'platform_set'", 'to': "orm['vgdb.DistMethod']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_transferrable': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'platform': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'dist_method_set'", 'to': "orm['vgdb.Platform']"})
        }
    }

    complete_apps = ['vgdb']
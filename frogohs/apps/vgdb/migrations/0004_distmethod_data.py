# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import DataMigration
from django.db import models

class Migration(DataMigration):

    def forwards(self, orm):
        "Write your forwards methods here."
        # Note: Remember to use orm['appname.ModelName'] rather than "from appname.models..."
        
        orm.DistMethod(name='Physical Media', slug='physical-media').save()
                
        orm.DistMethod(name='Developer Website', slug='dev-website').save()
        
        orm.DistMethod(name='Steam', slug='steam').save()
        orm.DistMethod(name='Desura', slug='desura').save()
        orm.DistMethod(name='Good Old Games', slug='gog').save()
        orm.DistMethod(name='Origin', slug='origin').save()
        
        orm.DistMethod(name='Mac App Store', slug='mac-app-store').save()
        
        orm.DistMethod(name='PlayStation Network', slug='psn').save()
        orm.DistMethod(name='Xbox Live Marketplace', slug='xblm').save()
        
        orm.DistMethod(name='Wii Shop Channel', slug='wii-shop-channel').save()
        orm.DistMethod(name='Nintendo eShop', slug='nintendo-eshop').save()
        orm.DistMethod(name='DSi Shop', slug='dsi-shop').save()
                
    def backwards(self, orm):
        "Write your backwards methods here."
        
        orm.DistMethod.objects.filter(slug='physical-media').delete()
        
        orm.DistMethod.objects.filter(slug='dev-website').delete()
                
        orm.DistMethod.objects.filter(slug='steam').delete()
        orm.DistMethod.objects.filter(slug='desura').delete()
        orm.DistMethod.objects.filter(slug='gog').delete()
        orm.DistMethod.objects.filter(slug='origin').delete()
        
        orm.DistMethod.objects.filter(slug='mac-app-store').delete()
        
        orm.DistMethod.objects.filter(slug='psn').delete()
        orm.DistMethod.objects.filter(slug='xblm').delete()
        
        orm.DistMethod.objects.filter(slug='wii-shop-channel').delete()
        orm.DistMethod.objects.filter(slug='nintendo-eshop').delete()   
        orm.DistMethod.objects.filter(slug='dsi-shop').delete()        
                       
    models = {
        'vgdb.distmethod': {
            'Meta': {'ordering': "['name']", 'object_name': 'DistMethod'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '50'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'})
        },
        'vgdb.platform': {
            'Meta': {'ordering': "['name']", 'object_name': 'Platform'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '50'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'})
        }
    }

    complete_apps = ['vgdb']
    symmetrical = True

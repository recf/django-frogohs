# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import DataMigration
from django.db import models

class Migration(DataMigration):

    def forwards(self, orm):
        "Write your forwards methods here."
        # Note: Remember to use orm['appname.ModelName'] rather than "from appname.models..."
        
        orm.Platform(name='Xbox 360', slug='xbox-360').save()
        orm.Platform(name='Playstation 3', slug='ps3').save()
        orm.Platform(name='Wii', slug='wii').save()
        orm.Platform(name='Wii U', slug='wii-u').save()
        
        orm.Platform(name='Windows', slug='windows').save()
        orm.Platform(name='Mac OS', slug='mac').save()
        orm.Platform(name='Linux', slug='linux').save()
        
        orm.Platform(name='Nintendo DS', slug='ds').save()
        orm.Platform(name='Nintendo 3DS', slug='3ds').save()
        orm.Platform(name='Playstation Portable', slug='psp').save()
        orm.Platform(name='Playstation Vita', slug='vita').save()

    def backwards(self, orm):
        "Write your backwards methods here."

        orm.Platform.objects.filter(slug='xbox-360').delete()
        orm.Platform.objects.filter(slug='ps3').delete()
        orm.Platform.objects.filter(slug='wii').delete()
        orm.Platform.objects.filter(slug='wii-u').delete()
        
        orm.Platform.objects.filter(slug='windows').delete()
        orm.Platform.objects.filter(slug='mac').delete()
        orm.Platform.objects.filter(slug='linux').delete()
        
        orm.Platform.objects.filter(slug='ds').delete()
        orm.Platform.objects.filter(slug='3ds').delete()
        orm.Platform.objects.filter(slug='psp').delete()
        orm.Platform.objects.filter(slug='vita').delete()        

    models = {        
        'vgdb.platform': {
            'Meta': {'ordering': "['name']", 'object_name': 'Platform'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '50'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'})
        }
    }

    complete_apps = ['vgdb']
    symmetrical = True

# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Removing unique constraint on 'GameContent', fields ['subtitle', 'title']
        db.delete_unique('vgdb_gamecontent', ['subtitle', 'title'])

        # Deleting field 'GameContent.subtitle'
        db.delete_column('vgdb_gamecontent', 'subtitle')

        # Deleting field 'GameContent.title'
        db.delete_column('vgdb_gamecontent', 'title')

        # Deleting field 'GameContent.series'
        db.delete_column('vgdb_gamecontent', 'series_id')

        # Adding unique constraint on 'GameContent', fields ['game', 'name']
        db.create_unique('vgdb_gamecontent', ['game_id', 'name'])


    def backwards(self, orm):
        # Removing unique constraint on 'GameContent', fields ['game', 'name']
        db.delete_unique('vgdb_gamecontent', ['game_id', 'name'])

        # Adding field 'GameContent.subtitle'
        db.add_column('vgdb_gamecontent', 'subtitle',
                      self.gf('django.db.models.fields.CharField')(default='n/a', max_length=50),
                      keep_default=False)

        # Adding field 'GameContent.title'
        db.add_column('vgdb_gamecontent', 'title',
                      self.gf('django.db.models.fields.CharField')(default='n/a', max_length=50),
                      keep_default=False)

        # Adding field 'GameContent.series'
        db.add_column('vgdb_gamecontent', 'series',
                      self.gf('django.db.models.fields.related.ForeignKey')(to=orm['vgdb.Series'], null=True, blank=True),
                      keep_default=False)

        # Adding unique constraint on 'GameContent', fields ['subtitle', 'title']
        db.create_unique('vgdb_gamecontent', ['subtitle', 'title'])


    models = {
        'vgdb.distmethod': {
            'Meta': {'ordering': "['name']", 'object_name': 'DistMethod'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '50'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'})
        },
        'vgdb.game': {
            'Meta': {'ordering': "['series', 'name']", 'object_name': 'Game'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'series': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['vgdb.Series']", 'null': 'True', 'blank': 'True'})
        },
        'vgdb.gamecontent': {
            'Meta': {'ordering': "['name', 'game']", 'unique_together': "(('name', 'game'),)", 'object_name': 'GameContent'},
            'game': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['vgdb.Game']", 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'max_players': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'min_players': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True'})
        },
        'vgdb.platform': {
            'Meta': {'ordering': "['name']", 'object_name': 'Platform'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '50'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'})
        },
        'vgdb.platformdistmethod': {
            'Meta': {'ordering': "['platform', 'dist_method']", 'unique_together': "(('platform', 'dist_method'),)", 'object_name': 'PlatformDistMethod'},
            'dist_method': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'platform_set'", 'to': "orm['vgdb.DistMethod']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_transferrable': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'platform': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'dist_method_set'", 'to': "orm['vgdb.Platform']"})
        },
        'vgdb.region': {
            'Meta': {'ordering': "['name']", 'object_name': 'Region'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '50'}),
            'slug': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '10'})
        },
        'vgdb.release': {
            'Meta': {'object_name': 'Release'},
            'boxart': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'platform_distmethod': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['vgdb.PlatformDistMethod']"}),
            'region': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['vgdb.Region']"}),
            'release_date': ('django.db.models.fields.DateField', [], {})
        },
        'vgdb.series': {
            'Meta': {'ordering': "['name']", 'object_name': 'Series'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '50'}),
            'parent_series': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'subseries_set'", 'null': 'True', 'to': "orm['vgdb.Series']"})
        }
    }

    complete_apps = ['vgdb']
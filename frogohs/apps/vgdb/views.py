from django.shortcuts import render
from django.views.decorators.http import require_safe

from django.views.generic.list import ListView
from django.views.generic.detail import DetailView

from models import Platform, \
                   DistMethod, \
                   Series, \
                   Game, \
                   GameContent

def template_name(request, model, view_type, ext = 'html'):
    return "%(app_name)s/%(model)s_%(view_type)s.%(ext)s" % \
               {'app_name': 'vgdb',
                'model': model.__name__.lower(),
                'view_type': view_type,
                'ext': ext}

def render_list(request, model, **kwargs):
    return render(request, 
                  template_name(request, model, 'list'),
                  kwargs)

def render_detail(request, model, **kwargs):
    return render(request, 
                  template_name(request, model, 'detail'),
                  kwargs)

@require_safe    
def platform_list_get(request):
    platform_list = Platform.objects.all()

    return render_list(request, Platform, platform_list=platform_list)

@require_safe
def platform_detail_get(request, platform_slug):
    platform = Platform.objects.get(slug=platform_slug)

    return render_detail(request, Platform, platform=platform)
       
@require_safe
def distmethod_list_get(request):
    distmethod_list = DistMethod.objects.all()

    return render_list(request, DistMethod, distmethod_list=distmethod_list)

@require_safe
def distmethod_detail_get(request, distmethod_slug):
    distmethod = DistMethod.objects.get(slug=distmethod_slug)

    return render_detail(request, DistMethod, distmethod=distmethod)

@require_safe
def series_list_get(request):
    series_list = Series.objects.all()

    return render_list(request, Series, series_list=series_list)

@require_safe
def series_detail_get(request, series_id):
    series = Series.objects.get(pk=series_id)

    return render_detail(request, Series, series=series)

@require_safe
def game_detail_get(request, game_id):
    game = Game.objects.get(pk=game_id)

    return render_detail(request, Game, game=game)
    

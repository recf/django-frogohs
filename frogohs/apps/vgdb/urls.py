from django.conf.urls.defaults import patterns, include, url

import views

urlpatterns = patterns('',
    
    url(r'^platforms/$', 
#        views.PlatformListView.as_view(),
        views.platform_list_get,
        name='vgdb-platform-list'),
        
    url(r'^platforms/(?P<platform_slug>[\w-]+)/$', 
#        views.PlatformDetailView.as_view(), 
        views.platform_detail_get,
        name='vgdb-platform-detail'),
        
    url(r'^distmethods/$', 
#        views.DistMethodListView.as_view(),
        views.distmethod_list_get,
        name='vgdb-distmethod-list'),
        
    url(r'^distmethods/(?P<distmethod_slug>[\w-]+)/$', 
#        views.DistMethodDetailView.as_view(),
        views.distmethod_detail_get,
        name='vgdb-distmethod-detail'),
        
    url(r'^series/$', 
#        views.SeriesListView.as_view(),
        views.series_list_get,
        name='vgdb-series-list'),
        
    url(r'^series/(?P<series_id>\d+)/$', 
#        views.SeriesDetailView.as_view(),
        views.series_detail_get,
        name='vgdb-series-detail'),

    url(r'^game/(?P<game_id>\d+)/$',
        views.game_detail_get,
        name='vgdb-game-detail'),
)


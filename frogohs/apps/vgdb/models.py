from django.db import models
import managers

# Platforms and Distribution Methods
class Platform(models.Model):
    objects = managers.PlatformManager()

    name = models.CharField(max_length=50, unique=True)
    slug = models.SlugField(max_length=50, unique=True)
    
    def natural_key(self):
        return (self.slug,)
    
    def __unicode__(self):
        return self.name
        
    class Meta:
        ordering = ['name']

class DistMethod(models.Model):
    
    objects = managers.DistMethodManager()
    
    name = models.CharField(max_length=50, unique=True)
    slug = models.SlugField(max_length=50, unique=True)
    
    def natural_key(self):
        return (self.slug,)
    
    def __unicode__(self):
        return self.name
    
    class Meta:
        ordering = ['name']
        verbose_name = 'distribution method'
        verbose_name_plural = 'distribution methods'

class PlatformDistMethod(models.Model):
    platform = models.ForeignKey(Platform, related_name='dist_method_set')
    dist_method = models.ForeignKey(DistMethod, related_name='platform_set')
    is_transferrable = models.BooleanField()
    
    def __unicode__(self):
        return u"%(platform)s via %(dist_method)s" % {"platform" : self.platform,
                                                      "dist_method" : self.dist_method,}
    
    class Meta:
        unique_together = (('platform', 'dist_method'),)
        ordering = ['platform', 'dist_method']
        
# Series, Game content and Releases
class Series(models.Model):
    parent_series = models.ForeignKey('self', 
                                      blank=True, 
                                      null=True,
                                      related_name='subseries_set')
    name = models.CharField(max_length=50, unique=True)
    
    def __unicode__(self):
        return self.name
        
    class Meta:
        ordering = ['name']
        verbose_name_plural = 'series'

class Game(models.Model):
    series = models.ForeignKey(Series, blank=True, null=True)
    name = models.CharField(max_length=50)

    def __unicode__(self):
        return self.name
        
    class Meta:
        ordering = ['series', 'name']
        
class GameContent(models.Model):

    game = models.ForeignKey(Game,null=True)
    name = models.CharField(max_length=50,null=True)

    min_players = models.IntegerField(default=1)
    max_players = models.IntegerField(default=1)
    
    def __unicode__(self):
        return self.name
        
    class Meta:
        ordering = ['name', 'game']
        unique_together = (('name', 'game',),)

class Region(models.Model):
    slug = models.CharField(max_length=10, unique=True)
    name = models.CharField(max_length=50, unique=True)
    
    def __unicode__(self):
        return self.name
        
    class Meta:
        ordering = ['name']

class Release(models.Model):
    name = models.CharField(max_length=50)
    region = models.ForeignKey(Region)
    platform_distmethod = models.ForeignKey(PlatformDistMethod)
    release_date = models.DateField()
    boxart = models.ImageField(upload_to='vgdb/boxart')

#class ReleaseGame(models.Model):
#    release = models.ForeignKey(Release)
#    game = models.ForeignKey(Game)


from django.conf.urls.defaults import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'frogohs.views.home', name='home'),
    # url(r'^frogohs/', include('frogohs.foo.urls')),
	
    url(r'^project/', include('frogohs.apps.project.urls')),

    url(r'^vgdb/', include('frogohs.apps.vgdb.urls')),
    
    url(r'^collmgt/', include('frogohs.apps.collmgt.urls')),
    
    # Uncomment the admin/doc line below to enable admin documentation:
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
    
    url(r'^accounts/', include('registration.backends.default.urls')),
)

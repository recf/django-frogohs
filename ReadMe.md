# Frogohs Project

Just copying the orginal post from GWJ for now:

I have had this gaming site idea stuck in my head for like 4 years now, and I think I've finally hit the point where I need to make it, if for no other reason than to be able to stop thinking about it. Unfortunately, it isn't terribly original. Most of the features I have in mind exist in one site or another, but I haven't seem them all in the same site. So yes, I think it would be useful, but it would require a ton of work just to get parity with other sites, before even working on the stuff that is original (or at least less common).

If I'm actually going to get a site live this time (rather than languish after a couple weeks like I usually do), I need to keep motivated and focused.

For keeping focused, I'm going to work in thin horizontal slices (e.g. data-model, UI for admin, search, list and detail for platforms). Before I've typically done vertical slices (e.g. the entire data model) and it's led to burn out.

For keeping motivated, I want to be working on the right things. For first release (whenever the hell that ends up being), I want to find the minimum set of useful functionality. For that, check out the issues list. Feel free to comment, or suggest new ideas.
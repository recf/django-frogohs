#! /bin/bash

sudo apt-get install python
sudo apt-get install python-dev # required for PIL/pillow
sudo apt-get install postgresql # 9.1
sudo apt-get install python-psycopg2
sudy apt-get install ruby

echo 'Checking for Python VirtualEnvWrapper'
if pip freeze | grep virtualenvwrapper 2>&1 > /dev/null 
then
    echo 'VirtualEnvWrapper is already installed'
else
    sudo pip install virtualenvwrapper
fi

# Create Virtual Env root
if [ $WORKON_HOME ]
then
    echo 'VirtualEnvWrapper is already configured'
else
    echo 'Configuring VirtualEnvWrapper'
    PROFILE=~/.bash_profile

    read -e -p "Where do you want to store your virtual environments?: " -i '$HOME/.virtualenvs' NEW_WORKON_HOME

    echo "Adding configuration to $PROFILE"

    echo '' >> $PROFILE
    echo '# Python Virtual Env Wrapper Configuration' >> $PROFILE
    echo "export WORKON_HOME=$NEW_WORKON_HOME" >> $PROFILE
    echo '. /usr/local/bin/virtualenvwrapper.sh' >> $PROFILE

    echo "Updating configuration for current shell"
    export WORKON_HOME=$NEW_WORKON_HOME
fi

. /usr/local/bin/virtualenvwrapper.sh

# Create Virtual Env
ENVNAME=django-frogohs
if lsvirtualenv | grep $ENVNAME 2>&1 > /dev/null
then
    echo "Virtual Env '$ENVNAME' found"
else
    echo "Virtual Env '$ENVNAME' does not exist. Creating it."
    mkvirtualenv $ENVNAME
fi

sudo apt-get install ruby
sudo apt-get install ruby-oily-png # improves spriting in compass
sudo gem install sass
sudo gem install compass

